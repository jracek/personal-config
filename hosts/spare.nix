{ config, pkgs, lib, ft, ... }:

{
  imports = [
    "hardware-configuration.nix"
    "profiles/users/jakub.nix"
    "profiles/thinkpad.nix"
    "profiles/browsers.nix"
    "profiles/comm.nix"
    "profiles/desktop.nix"
    "profiles/editor.nix"
    "profiles/fonts.nix"
    "profiles/keybase.nix"
    "profiles/music.nix"
    "profiles/network.nix"
    "profiles/virtualization.nix"
  ];

  # Bootloader
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    initrd.luks.devices = [
      {
        name = "root";
        device = "/dev/disk/by-uuid/e5339add-4faf-41a6-bf92-f49569b0615b";
        preLVM = true;
      }
    ];
};

  networking.firewall.allowedTCPPortRanges = [
    { from = 4100; to = 4105; }
  ];

  networking.firewall.allowedUDPPorts = [
    5353
  ];

  networking.firewall.allowedTCPPorts = [
    3000
    4000
    5000
    9001
    1337
  ];
  
  # Extra packages
  environment.systemPackages = with pkgs; [
  ];

  # Enable ssh, disable password
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
  };
}