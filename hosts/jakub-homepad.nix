{ config, pkgs, lib, ft, ... }:

{
  imports = [
    "hardware-configuration.nix"
    "profiles/users/jakub.nix"
    "profiles/thinkpad.nix"
    "profiles/browsers.nix"
    "profiles/comm.nix"
    "profiles/desktop.nix"
    "profiles/editor.nix"
    "profiles/fonts.nix"
    "profiles/graphics.nix"
    "profiles/keybase.nix"
    "profiles/movies.nix"
    "profiles/music.nix"
    "profiles/network.nix"
    "profiles/virtualization.nix"
    "profiles/direnv.nix"
  ];

  # Bootloader
  boot.loader = {
    grub.enable = true;
    grub.version = 2;
    grub.device = "/dev/sda";
  };

  networking.firewall.allowedTCPPortRanges = [
    { from = 4100; to = 4105; }
  ];

  networking.firewall.allowedUDPPorts = [
    5353
  ];

  networking.firewall.allowedTCPPorts = [
    3000
    4000
    5000
    9001
    1337
  ];
  
  # Extra packages
  environment.systemPackages = with pkgs; [
  ];

  # Enable ssh, disable password
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
  };
}
