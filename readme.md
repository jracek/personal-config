# GPG

## import GPG Key

```
keyabse pgp export | gpg --import
keybase pgp export -s | gpg --allow-secret-key-import --import
```

## Create new ssh key

```
# Get key-id
gpg --list-secret-keys --keyid-format LONG

gpg2 --expert --edit-key key-id

addkey

# Now select 8 -> S -> E -> A for auth only -> Q -> 4096

gpg --list-keys --with-keygrip

# Add keygrip to ~/.gnupg/sshcontrol

# to list the public key

ssh-add -L 
```
