{ config, lib, pkgs, ... }:

{
  powerManagement.cpuFreqGovernor = "powersave";
  powerManagement.powertop.enable = true;

  hardware.opengl = {
    extraPackages = [ pkgs.vaapiIntel ];
    s3tcSupport = true;
  };

  hardware.bluetooth = {
    enable = true;
  };

  hardware.pulseaudio = {
    # required for bluetooth package
    package = pkgs.pulseaudioFull;
    extraModules = [ pkgs.pulseaudio-modules-bt ];
  };

  services.logind.lidSwitch = "suspend";

  services.xserver = {
    videoDrivers = [ "modesetting" ];
    useGlamor = true;

    synaptics.enable = false;

    libinput = {
      enable = true;
      naturalScrolling = true;
    };
  };
}
