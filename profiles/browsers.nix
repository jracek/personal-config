{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    brave
    chromium
    ranger
  ];

  environment.variables = {
    BROWSER = pkgs.lib.mkOverride 0 "brave";
  };

  networking.firewall.allowedUDPPortRanges = [
    { from = 32768; to = 61000; }
  ];

  networking.firewall.allowedTCPPortRanges = [
    { from = 8008; to = 8009; }
  ];

  networking.firewall.allowedUDPPorts = [
    1900
  ];
}
