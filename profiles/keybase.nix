{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    keybase-gui
    gnupg
    monkeysphere
  ];

  services.kbfs.enable = true;
  services.keybase.enable = true;

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
}
