{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    qbittorrent
    #vlc
  ];

  networking.firewall.allowedTCPPorts = [
    8010 #vlc
  ];

  nixpkgs.config.packageOverrides = pkgs: {
    vlc = pkgs.vlc.overrideAttrs(old: rec {
      # Protobuf is required for Chromecast support
      buildInputs = old.buildInputs ++ [ pkgs.protobuf ];
    });
  };
}
