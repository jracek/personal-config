{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    openvpn
    networkmanager_openvpn
    wget
    curl
    whois
    speedtest-cli
    nix-prefetch-git
  ];

  networking.extraHosts =
  ''
    192.168.56.101 drone.smaply.com
  '';

  networking.usePredictableInterfaceNames = false;
  networking.firewall.enable = true;

  networking.networkmanager = {
    enable = true;
    packages = with pkgs; [ networkmanager_openvpn ];
  };

  # Enable ssh, disable password
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
  };
}
