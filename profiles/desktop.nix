{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    rofi
    flameshot
    xclip
    feh
    polybar
    xlockmore
    xorg.xrandr

    # Gnome stuff
    equilux-theme
  ];

  environment.gnome3.excludePackages = with pkgs; [
    gnome3.gnome-panel
  ];

  # Mouse
  services.gpm.enable = true;

  # Sound
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  services.xserver = {
    enable = true;

    # Swap caps for ctrl
    xkbOptions = "ctrl:nocaps";
    layout = "us";

    displayManager = {
      lightdm.enable = true;
    };

    windowManager = {
      xmonad.enable = true;
      default = "xmonad";
    };

    desktopManager = {
      gnome3 = {
        enable = true;
        flashback.customSessions = [
          {
            wmCommand = "${pkgs.haskellPackages.xmonad}/bin/xmonad";
            wmLabel = "XmonadFallback";
            wmName = "xmonad-fallback";
          }
        ];
      };
      xfce.enable = false;
      default = "gnome3";
    };
  };

  #Multi screens
  services.autorandr.enable = true;

  # screen locking
  services.xserver.xautolock = {
    enable = true;
    locker = "${pkgs.lightdm}/bin/dm-tool switch-to-greeter";
    extraOptions = [ "-detectsleep" ];
    killer = "${pkgs.systemd}/bin/systemctl suspend";
    killtime = 10; # 10 is minimal value
    time = 5;
  };

  # xss-lock subscribes to the systemd-events suspend, hibernate, lock-session,
  # and unlock-session with appropriate actions (run locker and wait for user to unlock or kill locker).
  # xss-lock also reacts to DPMS events and runs or kills the locker in response.
  programs.xss-lock.enable = true;
  programs.xss-lock.lockerCommand = "${pkgs.lightdm}/bin/dm-tool switch-to-greeter";

}
