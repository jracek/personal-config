{ config, lib, pkgs, __nixPath, ft, ... }:

let
  inherit (lib) optional optionals optionalAttrs;
in
{
  networking.networkmanager.enable = true;
  environment.systemPackages = with pkgs; [
    htop
    killall
    gcc
    lorri
  ];
  environment.shellAliases.top = "htop";
  services.printing.enable = true;

  # Yaay
  nixpkgs.config.allowUnfree = true;


  time.timeZone = "Europe/Prague";
  i18n = {
    defaultLocale = "en_US.UTF-8";
    consoleUseXkbConfig = true;
  };

  system.stateVersion = "19.03";

  # Go with garbage, optimize and upgrade from time to time
  nix.gc = {
    automatic = true;
    options = "--delete-older-than 14d";
  };
  nix.optimise.automatic = true;
  system.autoUpgrade.enable = true;
}
