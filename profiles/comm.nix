{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    slack
    discord
    wire-desktop
    termite
  ];
}
