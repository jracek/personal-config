{ pkgs, ... }:

{
  users.extraUsers.jakub = {
    description = "Jakub Racek <me@jakubracek.net>";
    isNormalUser = true;
    uid = 1000;
    group = "users";
    extraGroups = [
      "wheel"
      "networkmanager"
      "docker"
    ];
    createHome = true;
    initialPassword = "rootroot";
    useDefaultShell = true;
  };

  home-manager.users.jakub = { ... }: {

    home.packages = [
      #Screenshot
      pkgs.flameshot

      # Doom
      pkgs.gnutls
      pkgs.aspell
      pkgs.editorconfig-core-c

      # Global langs
      pkgs.ruby
      pkgs.ripgrep
      pkgs.fd
    ];

    imports = [
      ../home/git.nix
      ../home/home.nix
      ../home/xmonad.nix
      ../home/emacs.nix
      ../home/rofi.nix
    ];
  };
}
