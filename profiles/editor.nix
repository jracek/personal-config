{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    vim
    emacs
    ruby
    ripgrep
    fd
    ag
    file

    #overlay
    lorri
  ];
}
