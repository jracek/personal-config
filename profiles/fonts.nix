{ pkgs, ...}:

{
  fonts = {
    fonts = with pkgs; [
      dina-font
      siji
      unifont
      fira
      fira-mono
      fira-code
      emojione
      noto-fonts-emoji
      emacs-all-the-icons-fonts
      google-fonts
    ];

    enableFontDir = true;
  };
}
