--------------------------------------------------------------------------------
-- Xmonad.hs
--------------------------------------------------------------------------------
-- Pragma
{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances #-}

--------------------------------------------------------------------------------
-- Imports
--------------------------------------------------------------------------------
-- System
import System.IO
import System.Exit

-- Xmonad
import XMonad
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Config
import XMonad.Config.Desktop
import XMonad.Actions.CycleWS
import XMonad.Hooks.ManageDocks
import XMonad.Layout.PerWorkspace (onWorkspace, onWorkspaces)
import XMonad.Layout.Spacing
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.UrgencyHook
import XMonad.Layout.ResizableTile
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.Decoration
import XMonad.Util.Types
import qualified XMonad.StackSet as W

-- Keys
import Graphics.X11.ExtraTypes.XF86
--
-- Data
import qualified Data.Map

--------------------------------------------------------------------------------
-- Settings
-------------------------------------------------------------------------------- 
-- Look and feel
myBorderWidth        = 0
-- Colors
myFocusedBorderColor = "#fabd2f"
myNormalBorderColor  = "#928374"

-- Side decoration
data SideDecoration a = SideDecoration Direction2D
    deriving (Show, Read)

instance Eq a => DecorationStyle SideDecoration a where

    shrink b (Rectangle _ _ dw dh) (Rectangle x y w h)
        | SideDecoration U <- b = Rectangle x (y + fi dh) w (h - dh)
        | SideDecoration R <- b = Rectangle x y (w - dw) h
        | SideDecoration D <- b = Rectangle x y w (h - dh)
        | SideDecoration L <- b = Rectangle (x + fi dw) y (w - dw) h

    pureDecoration b dw dh _ st _ (win, Rectangle x y w h)
        | win `elem` W.integrate st && dw < w && dh < h = Just $ case b of
        SideDecoration U -> Rectangle x y w dh
        SideDecoration R -> Rectangle (x + fi (w - dw)) y dw h
        SideDecoration D -> Rectangle x (y + fi (h - dh)) w dh
        SideDecoration L -> Rectangle x y dw h
        | otherwise = Nothing

myTheme :: Theme
myTheme = defaultTheme {
    activeColor = "#fabd2f",
    activeBorderColor = "#fabd2f",
    inactiveColor = "#282828",
    inactiveBorderColor = "#282828",
    decoWidth = 15
}

myDecorate :: Eq a => l a -> ModifiedLayout (Decoration SideDecoration DefaultShrinker) l a
myDecorate = decoration shrinkText myTheme (SideDecoration L)

-- Settings
myTerminal = "termite -t terminal"

--------------------------------------------------------------------------------
-- Workspaces
--------------------------------------------------------------------------------
myWorkspaces = ["net", "chat", "admin", "srv", "media", "code", "misc"]

--------------------------------------------------------------------------------
-- Manage hook
--------------------------------------------------------------------------------
myManageHook = composeAll
   [
    -- net
    className =? "Termite" <&&> title =? "neomutt" --> doShift "net",
    -- chat
    className =? "Keybase"                         --> doShift "chat",
    className =? "discord"                         --> doShift "chat",
    className =? "Slack"                           --> doShift "chat",
    title     =? "Telegram"                        --> doShift "chat",
    className =? "Mattermost"                      --> doShift "chat",
    className =? "Riot"                            --> doShift "chat",
    className =? "Termite" <&&> title =? "weechat" --> doShift "chat",
    -- experimental, just for spotify...
    className =? ""                                --> doShift "media",
    -- code
    className =? "code-oss"                        --> doShift "code",
    className =? "Electron2"                       --> doCenterFloat,
    -- misc
    className =? "Google-chrome-unstable"          --> doShift "misc",
    className =? "Thunderbird"                     --> doShift "misc",
    role      =? "GtkFileChooserDialog"            --> doCenterFloat,
    manageDocks
   ]

    where

        role      = stringProperty "WM_WINDOW_ROLE"
        name      = stringProperty "WM_NAME"

--------------------------------------------------------------------------------
-- Scratchpads
--------------------------------------------------------------------------------
myScratchPads = [
                 NS "terminal" spawnTerm findTerm manageTerm,
                 NS "mixer"    spawnMixer findMixer manageMixer
                ]
  where
    spawnTerm = myTerminal ++ " --class=scratchpad"
    findTerm = className =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
      where
        h = 0.6
        w = 0.6
        t = (1 - h)/2
        l = (1 - w)/2
    spawnMixer = "pavucontrol"
    findMixer = className =? "Pavucontrol"
    manageMixer = customFloating $ W.RationalRect l t w h
      where
        h = 0.6
        w = 1
        t = 1 - h
        l = (1 - w)/2


--------------------------------------------------------------------------------
-- Layouts
--------------------------------------------------------------------------------
-- default
myDefaultLayout = myDecorate (avoidStruts $ tiled ||| Mirror tiled ||| Full)
  where
    tiled = smartSpacing 5 $ ResizableTall 1 (2/100) (1/2) []
--
-- fullscreen
myFsLayout = avoidStruts $ Full ||| Mirror tiled ||| tiled
  where
    tiled = smartSpacing 5 $ ResizableTall 1 (2/100) (1/2) []

myLayoutHook = onWorkspaces["net","chat","media","misc"] myFsLayout myDefaultLayout

--------------------------------------------------------------------------------
-- Bars
--------------------------------------------------------------------------------

-- XmonadBar
myXmonadBar = "polybar -c .xmonad/bar/config mainbar-xmonad"

-- LogHook
myLogHook :: Handle -> X ()
myLogHook h = dynamicLogWithPP $ def
    {
        ppCurrent           =   dzenColor "#d79921" "#282828" . pad
      , ppVisible           =   dzenColor "#657b83" "#282828" . pad
      , ppHidden            =   dzenColor "#ebdbb2" "#282828" . pad . noScratchPad
      , ppHiddenNoWindows   =   dzenColor "#7b7b7b" "#282828" . pad . noScratchPad
      , ppUrgent            =   dzenColor "#dc322f" "#282828" . pad
      , ppWsSep             =   " "
      , ppSep               =   "  |  "
      , ppLayout            =   dzenColor "#98971a" "#282828" .
                                (\x -> case x of
                                    "SideDecoration L Spacing ResizableTall"        ->      "^i(" ++ myIconsDir ++ "/tall.xbm)"
                                    "SideDecoration L Spacing Tall"                 ->      "^i(" ++ myIconsDir ++ "/tall.xbm)"
                                    "SideDecoration L Spacing Mirror Tall"          ->      "^i(" ++ myIconsDir ++ "/mtall.xbm)"
                                    "SideDecoration L Mirror Spacing ResizableTall" ->      "^i(" ++ myIconsDir ++ "/mtall.xbm)"
                                    "SideDecoration L Full"                         ->      "^i(" ++ myIconsDir ++ "/full.xbm)"
                                    "Full"                                          ->      "^i(" ++ myIconsDir ++ "/full.xbm)"
                                    "Mirror Spacing ResizableTall"                  ->      "^i(" ++ myIconsDir ++ "/mtall.xbm)"
                                    "Spacing ResizableTall"                         ->      "^i(" ++ myIconsDir ++ "/tall.xbm)"
                                    _                                               ->      x
                                )
      , ppTitle             =   (" " ++) . dzenColor "#ebdbb2" "#282828" . dzenEscape
      , ppOutput            =   hPutStrLn h
    }
    where
        myIconsDir = "/home/papey/.xmonad/icons"
        noScratchPad ws = if ws == "NSP" then "" else ws


--------------------------------------------------------------------------------
-- Keys
--------------------------------------------------------------------------------
-- Mod
myModMask :: KeyMask
myModMask = mod1Mask
myWinModMask :: KeyMask
myWinModMask = mod4Mask

-- Keys
myKeys conf@(XConfig {XMonad.modMask = modMask}) = Data.Map.fromList $
    [
    -- quit // restart
    ((modMask .|. shiftMask, xK_q                     ), io (exitSuccess)),
    ((modMask,               xK_q                     ), spawn "/usr/bin/xmonad --recompile && /usr/bin/xmonad --restart"),

    -- controls
    ((modMask .|. shiftMask, xK_c                     ), kill),
    ((modMask,               xK_space                 ), sendMessage NextLayout),
    ((modMask .|. shiftMask, xK_space                 ), setLayout $ XMonad.layoutHook conf),

    -- focus
    ((modMask,               xK_space                 ), sendMessage NextLayout),
    ((modMask .|. shiftMask, xK_space                 ), setLayout $ XMonad.layoutHook conf),
    ((modMask,               xK_b                     ), sendMessage ToggleStruts),
    ((modMask,               xK_Tab                   ), windows W.focusDown),
    ((modMask,               xK_j                     ), windows W.focusDown),
    ((modMask,               xK_k                     ), windows W.focusUp),
    ((modMask .|. shiftMask, xK_j                     ), windows W.swapDown),
    ((modMask .|. shiftMask, xK_k                     ), windows W.swapUp),
    ((modMask,               xK_Return                ), windows W.swapMaster),
    ((modMask,               xK_u                     ), withFocused $ windows . W.sink),
    ((modMask,               xK_l                     ), sendMessage Expand),
    ((modMask,               xK_h                     ), sendMessage Shrink),
    ((myWinModMask,          xK_i                     ), focusUrgent),
    ((myWinModMask,          xK_n                     ), refresh),
    ((myWinModMask,          xK_comma                 ), sendMessage (IncMasterN 1)),
    ((myWinModMask,          xK_semicolon             ), sendMessage (IncMasterN (-1))),

    -- terminal
    ((modMask .|. shiftMask, xK_Return                ), spawn $ XMonad.terminal conf),

    -- ssh terminal see https://github.com/thestinger/termite#terminfo
    ((modMask .|. shiftMask, xK_s                     ), spawn "urxvt -title ssh"),
    --
    -- Scratchpad
    ((modMask .|. shiftMask, xK_BackSpace             ), scratchTerm),
    ((modMask .|. shiftMask, xK_m                     ), scratchMixer),

    -- workspaces
    ((modMask,               xK_z                     ), moveTo Next (WSIs notNSP)),
    ((modMask,               xK_a                     ), moveTo Prev (WSIs notNSP)),

    -- apps
    ((myWinModMask,          xK_f                     ), spawn "vivaldi-stable"),
    ((myWinModMask,          xK_m                     ), spawn "termite -e neomutt"),
    ((myWinModMask,          xK_e                     ), spawn "termite -e ranger"),
    ((myWinModMask,          xK_v                     ), spawn "termite -e nvim"),
    ((myWinModMask,          xK_b                     ), spawn "termite -e tmux"),
    ((myWinModMask,          xK_r                     ), spawn "rofi-pass"),
    ((myWinModMask,          xK_space                 ), spawn "rofi -combi-modi window,drun -show combi -modi combi"),
    ((myWinModMask,          xK_s                     ), spawn "rofi -show ssh"),
    ((myWinModMask,          xK_g                     ), spawn "killall -USR1 redshift"),

    -- lock
    ((myWinModMask,          xK_l                     ), spawn "betterlockscreen -l"),
    ((0,                     xK_Super_R               ), spawn "/usr/bin/systemctl suspend"),

    -- volume
    ((0,                     xF86XK_AudioLowerVolume  ), spawn "pamixer -d 3"),
    ((0,                     xF86XK_AudioRaiseVolume  ), spawn "pamixer -i 3"),
    ((0,                     xF86XK_AudioMute         ), spawn "pamixer -t"),

    -- media
    ((0,                     xF86XK_AudioPlay         ), spawn "playerctl play-pause"),
    ((0,                     xF86XK_AudioNext         ), spawn "playerctl next"),
    ((0,                     xF86XK_AudioPrev         ), spawn "playerctl previous"),

    -- backlight
    ((0,                     xF86XK_MonBrightnessDown ), spawn "light -U 10"),
    ((0,                     xF86XK_MonBrightnessUp   ), spawn "light -A 10")

    ]

    ++
    -- workspaces
    [((m .|. modMask, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_ampersand, xK_eacute, xK_quotedbl, xK_apostrophe,
            xK_parenleft, xK_minus, xK_egrave, xK_underscore]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    -- screens
    [((m .|. modMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_i, xK_o, xK_p] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

    where
        scratchTerm = namedScratchpadAction myScratchPads "terminal"
        scratchMixer = namedScratchpadAction myScratchPads "mixer"
        notNSP = (return $ ("NSP" /=) . W.tag) :: X (WindowSpace -> Bool)

--------------------------------------------------------------------------------
-- Startup apps
--------------------------------------------------------------------------------
myStartupHook :: X()
myStartupHook = do
    -- background
    spawn "feh --bg-scale /home/papey/code/xmonad/wallpapers/space.png"
    spawn "xsetroot -cursor_name left_ptr"
    -- autocutsel
    spawn "autocutsel -fork"
    spawn "autocutsel -selection PRIMARY -fork"
    spawn "xrdb -load .Xresources"
    -- redshift
    spawn "redshift -l 48.8:-0.7"
    -- unclutter
    spawn "unclutter"
    -- dunst
    spawn "dunst"

--------------------------------------------------------------------------------
-- Main
--------------------------------------------------------------------------------
main = do
    bar <- spawnPipe myXmonadBar
    xmonad $ ewmh $ docks $ withUrgencyHook NoUrgencyHook $ def
        {
            -- terminal
            terminal           = myTerminal,
            -- look and feel
            borderWidth        = myBorderWidth,
            normalBorderColor  = myNormalBorderColor,
            focusedBorderColor = myFocusedBorderColor,
            -- workspaces
            workspaces         = myWorkspaces,
            -- keys
            modMask            = myModMask,
            keys               = myKeys,
            -- logHook
            handleEventHook    = docksEventHook <+> handleEventHook desktopConfig,
            logHook            = myLogHook bar,
            -- bar
            manageHook         = myManageHook <+> namedScratchpadManageHook myScratchPads <+> manageHook def,
            layoutHook         = myLayoutHook,
            -- no follow mouse
            focusFollowsMouse  = False,
            -- start apps
            startupHook        = myStartupHook <+> docksStartupHook
        }
