{ ... }:

{
  programs.git = {
    enable = true;
    userName = "kubaracek";
    userEmail = "me@jakubracek.net";

    signing = {
      key = "0D84B97EA8351714";
      signByDefault = true;
    };

    ignores = [
      "*.todo"
    ];

    extraConfig = {
    };

    lfs.enable = true;
  };
}
